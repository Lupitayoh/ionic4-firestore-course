
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import * as firebase from 'firebase';

var config = {
  apiKey: "AIzaSyCOZPS905HQd2Nkw1YlB_hTsiZgtfrE8T0",
  authDomain: "feedlyapp-dcc04.firebaseapp.com",
  databaseURL: "https://feedlyapp-dcc04.firebaseio.com",
  projectId: "feedlyapp-dcc04",
  storageBucket: "feedlyapp-dcc04.appspot.com",
  messagingSenderId: "390603001437"
};
firebase.initializeApp(config);

@NgModule({
  declarations: [
    AppComponent
  ],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
