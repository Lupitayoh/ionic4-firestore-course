
import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  name: string = "";
  email: string = "";
  password: string = "";

  constructor() { }

  signup(){
    console.log(this.name);
    firebase.auth().createUserWithEmailAndPassword(this.email, this.password)
      .then((user) => {
        console.log(user);
      }).catch((err) => {
        console.log(err)
      });
  }

  ngOnInit() {
  }

}
